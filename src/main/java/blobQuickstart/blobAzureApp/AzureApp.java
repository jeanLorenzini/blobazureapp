package blobQuickstart.blobAzureApp;


import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.BlobRequestOptions;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class AzureApp extends Application
{
	/* *************************************************************************************************************************
	* Instructions: Start an Azure storage emulator, such as Azurite, before running the app.
	*    Alternatively, remove the "UseDevelopmentStorage=true;"; string and uncomment the 3 commented lines.
	*    Then, update the storageConnectionString variable with your AccountName and Key and run the sample.
	* *************************************************************************************************************************
	*/
	public static final String storageConnectionString = "UseDevelopmentStorage=true;";
	//"DefaultEndpointsProtocol=https;" +
	//"AccountName=<account-name>;" +
	//"AccountKey=<account-key>";

	public static void main( String[] args )
	{
	    launch();
	}
	
	private CloudStorageAccount storageAccount;
	private CloudBlobClient blobClient = null;
	private CloudBlobContainer container = null;
	
	@Override
    public void start(final Stage stage) throws InvalidKeyException, URISyntaxException {
        System.out.println("Azure Blob storage demo");

        // Parse the connection string and create a blob client to interact with Blob
        // storage
        storageAccount = CloudStorageAccount.parse(storageConnectionString);
        
        blobClient = storageAccount.createCloudBlobClient();
    
        Group root = new Group();
        Scene scene = new Scene(root, 640, 640);
        
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        
        Label label = new Label("Media App");
        label.setFont(new Font("Arial Bold", 25));
        GridPane.setHalignment(label, HPos.CENTER);
        gridpane.add(label, 0, 0);
        
        Label label2 = new Label("Enter Media Box name:");
        label2.setFont(new Font("Arial", 20));
        
        final TextField textField = new TextField ();
        HBox hb = new HBox();
        
        hb.getChildren().addAll(label2, textField);
        hb.setSpacing(10);
        gridpane.add(hb, 0, 10);
        
        Button submit = new Button("Click to create");
        GridPane.setConstraints(submit, 1, 10);
        gridpane.getChildren().add(submit);
        
        Button upload = new Button("Upload Blob !!!");
        GridPane.setConstraints(upload, 2, 10);
        gridpane.getChildren().add(upload);
        
        Button listBlobs = new Button("List Blobs!!!");
        GridPane.setConstraints(listBlobs, 1, 12);
        gridpane.getChildren().add(listBlobs);
        
        final ListView listView = new ListView();
        GridPane.setConstraints(listView, 0, 12);
        gridpane.getChildren().add(listView);
        
        submit.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                        public void handle(ActionEvent e) {
                            if ((textField.getText() != null && !textField.getText().isEmpty())) {
                                try {
                                    createContainer(textField.getText());
                                } catch (URISyntaxException e1) {
                                    e1.printStackTrace();
                                } catch (StorageException e1) {
                                    e1.printStackTrace();
                                }
                            }
                         }
                     }
                );
        
        final FileChooser fileChooser = new FileChooser();
        
        upload.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        File file = fileChooser.showOpenDialog(stage);
                        if (file != null) {
                            try {
                                uploadFile(file);
                            } catch (URISyntaxException e1) {
                                e1.printStackTrace();
                            } catch (StorageException e1) {
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });
        
        listBlobs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent e) {
                listBlobs(listView);
            }
        });

        root.getChildren().add(gridpane);
        
        stage.setScene(scene);
        stage.show();
    }
	
	//Create Azure Blob Container
	private void createContainer(String containerName) throws URISyntaxException, StorageException {
        container = blobClient.getContainerReference(containerName);

        // Create the container if it does not exist with public access.
        System.out.println("Creating container: " + container.getName());
        container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(), new OperationContext());
	}
	
	//Upload file to Azure Container
    private void uploadFile(File file) throws URISyntaxException, StorageException, IOException {
        if(container != null) {
            //Getting a blob reference
            CloudBlockBlob blob = container.getBlockBlobReference(file.getName());
    
            //Creating blob and uploading file to it
            System.out.println("Uploading the file ");
            blob.uploadFromFile(file.getAbsolutePath());
        } else {
            //TODO create a popup to display this error msg
            System.out.println("You need to create container first");
        }
	}
    
    //List Azure Container Files
    //TODO make it a list of buttons to download resource back
    private void listBlobs(ListView listView) {
        if(container != null) {
            listView.getItems().removeAll();
            // Listing contents of container
            for (ListBlobItem blobItem : container.listBlobs()) {
                listView.getItems().add(blobItem.getUri().getPath());
            }
        } else {
            //TODO create a popup to display this error msg
            System.out.println("You need to create container first");
        }
    }
	
}
